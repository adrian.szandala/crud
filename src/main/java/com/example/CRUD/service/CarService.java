package com.example.CRUD.service;

import com.example.CRUD.dto.CarDto;
import com.example.CRUD.model.Car;
import com.example.CRUD.repository.CarRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CarService {

    CarRepository carRepository;

    public List<Car> getCars() {
        return carRepository.findAll();
    }

    public Car createCar(CarDto carDto) {
        Car car = Car.builder()
                .name(carDto.getName())
                .color(carDto.getColor())
                .maxSpeed(carDto.getMaxSpeed())
                .build();
        return carRepository.save(car);
    }
}
